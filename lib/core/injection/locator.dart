import 'package:flutter_job_test/core/shared_preference/shared_preferences_manager.dart';
import 'package:flutter_job_test/weather/data/api_calls/weather_api_calls.dart';
import 'package:flutter_job_test/weather/data/repository/weather_repository.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

GetIt locator = GetIt.instance;

Future setupLocator() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

  SharedPreferencesManager sharedPreferencesManager =
      SharedPreferencesManager(sharedPreferences);

  locator.registerSingleton<SharedPreferencesManager>(
    sharedPreferencesManager,
  );

  locator.registerLazySingleton<WeatherRepository>(
    () => WeatherApiCalls(),
  );
}
