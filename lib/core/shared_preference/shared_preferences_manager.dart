import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesManager {
  SharedPreferences _sharedPreferences;

  SharedPreferencesManager(this._sharedPreferences);

  Future<bool> putBool(String key, bool value) =>
      _sharedPreferences.setBool(key, value);

  bool getBool(String key) {
    bool _result = _sharedPreferences.getBool(key);
    return _result == null ? false : _result;
  }

  Future<bool> putDouble(String key, double value) =>
      _sharedPreferences.setDouble(key, value);

  double getDouble(String key) => _sharedPreferences.getDouble(key);

  Future<bool> putInt(String key, int value) =>
      _sharedPreferences.setInt(key, value);

  int getInt(String key) => _sharedPreferences.getInt(key);

  Future<bool> putString(String key, String value) =>
      _sharedPreferences.setString(key, value);

  String getString(String key) => _sharedPreferences.getString(key);

  Future<bool> putStringList(String key, List<String> value) =>
      _sharedPreferences.setStringList(key, value);

  List<String> getStringList(String key) =>
      _sharedPreferences.getStringList(key);

  bool isKeyExists(String key) => _sharedPreferences.containsKey(key);

  Set<String> getAllKeys() => _sharedPreferences.getKeys();

  dynamic getAny(String key) => _sharedPreferences.get(key);

  Future<bool> clearKey(String key) => _sharedPreferences.remove(key);

  Future<bool> clearAll() => _sharedPreferences.clear();

  getAllKeysAndValues() {
    Set<String> _keys = _sharedPreferences.getKeys();
    _keys.forEach(
      (key) {
        print(
          key + ' : ' + _sharedPreferences.get(key).toString(),
        );
      },
    );
  }
}
