import 'dart:io';
import 'package:flutter_job_test/core/network/interceptor/add_session_key_and_language_interceptor.dart';
import 'package:http/io_client.dart' as http;
import 'package:chopper/chopper.dart';

class ApiService {
  static final chopper = ChopperClient(
    client: http.IOClient(
      HttpClient()..connectionTimeout = const Duration(seconds: 15),
    ),
    baseUrl: 'https://api.openweathermap.org/data/2.5/',
    services: [],
    converter: JsonConverter(),
    interceptors: [
      HttpLoggingInterceptor(),
      AddApiKeyAndLanguageKeyInterceptor(),
    ],
  );
}
