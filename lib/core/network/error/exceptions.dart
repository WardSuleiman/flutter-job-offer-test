import 'package:flutter/cupertino.dart';

const socketException = 'socket_exception';
const timeOutException = 'timeout_exception';
const notFoundException = 'notFound_exception';
const badResponseException = '/timeout_exception';
const unauthorizedException = 'unauthorized_exception';
const loginNeededException = 'login_needed_exception';

class BaseException implements Exception {
  String message;
  String type;

  BaseException({@required this.message, @required this.type});
}

class NotFoundException extends BaseException {
  NotFoundException()
      : super(
            message: 'Could not find what are you looking for',
            type: notFoundException);
}

class CustomSocketException extends BaseException {
  CustomSocketException()
      : super(
            message: 'Network is unreachable, Check your internet connection',
            type: socketException);
}

class CommunicationWithServerException extends BaseException {
  CommunicationWithServerException()
      : super(
            message: 'Failed to communicate with the server',
            type: timeOutException);
}

class BadRequestException extends BaseException {
  BadRequestException() : super(message: '', type: badResponseException);
}

class UnauthorizedException extends BaseException {
  UnauthorizedException()
      : super(message: 'Unauthorized', type: unauthorizedException);
}

class LoginNeededException extends BaseException {
  LoginNeededException()
      : super(message: 'Login needed', type: loginNeededException);
}
