import 'package:flutter/material.dart';
import 'package:flutter_job_test/core/network/models/city.dart';

class BaseResponse<T> {
  Status status;
  int message;
  T data;
  List<BaseResponseError> errors;
  City city;

  BaseResponse.completed(this.data,this.city) : status = Status.COMPLETED;
  BaseResponse.error(this.data, this.errors) : status = Status.ERROR;

  @override
  String toString() {
    return 'BaseResponse(status: $status, message: $message, data: $data, errors: $errors)';
  }
}

class BaseResponseError {
  String errorName;
  String errorMessage;

  BaseResponseError({
    @required this.errorName,
    @required this.errorMessage,
  });

  @override
  String toString() =>
      'BaseResponseError(errorName: $errorName, errorMessage: $errorMessage)';
}

enum Status { COMPLETED, ERROR }
