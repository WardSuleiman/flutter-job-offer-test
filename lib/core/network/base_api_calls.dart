import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:chopper/chopper.dart';
import 'package:flutter_job_test/core/network/base_response.dart';
import 'package:flutter_job_test/core/network/error/exceptions.dart';
import 'package:flutter_job_test/core/network/models/city.dart';

Future<BaseResponse> apiCall(Function apiCall) async {
  var _responseJson;
  try {
    Response response = await apiCall();

    _responseJson = _response(response);
    City _city = City.fromJson(_responseJson['city']);
    return BaseResponse.completed(_responseJson['list'], _city);
  } on SocketException {
    throw CustomSocketException();
  } on TimeoutException {
    throw CommunicationWithServerException();
  } on UnauthorizedException catch (e) {
    throw e;
  } on NotFoundException catch (e) {
    throw e;
  } on Exception catch (e) {
    throw BaseException(message: e.toString(), type: 'Server Error');
  }
}

dynamic _response(Response response) {
  switch (response.statusCode) {
    case 200:
      var responseJson = json.decode(
        response.bodyString,
      );
      return responseJson;
    case 401:
      throw UnauthorizedException();
    case 404:
      throw NotFoundException();
    case 500:
    default:
      throw Exception('Something went wrong!!');
  }
}
