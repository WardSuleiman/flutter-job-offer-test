import 'package:json_annotation/json_annotation.dart';

part 'rain.g.dart';

@JsonSerializable()
class Rain {
  @JsonKey(name: '3h')
  double d3h;

  Rain({
    this.d3h,
  });

  factory Rain.fromJson(Map<String, dynamic> json) => _$RainFromJson(json);
  Map<String, dynamic> toJson() => _$RainToJson(this);

  @override
  String toString() => 'Rain(d3h: $d3h)';
}
