// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'day_forecast.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DayForecast _$DayForecastFromJson(Map<String, dynamic> json) {
  return DayForecast(
    dt: json['dt'] as int,
    main: json['main'] == null
        ? null
        : Main.fromJson(json['main'] as Map<String, dynamic>),
    weather: (json['weather'] as List)
        ?.map((e) =>
            e == null ? null : Weather.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    clouds: json['clouds'] == null
        ? null
        : Cloud.fromJson(json['clouds'] as Map<String, dynamic>),
    wind: json['wind'] == null
        ? null
        : Wind.fromJson(json['wind'] as Map<String, dynamic>),
    visibility: json['visibility'] as int,
    pop: (json['pop'] as num)?.toDouble(),
    rain: json['rain'] == null
        ? null
        : Rain.fromJson(json['rain'] as Map<String, dynamic>),
    sys: json['sys'] == null
        ? null
        : TimeOfDay.fromJson(json['sys'] as Map<String, dynamic>),
    dtTxt: DayForecast._convertTODateTime(json['dt_txt']),
  );
}

Map<String, dynamic> _$DayForecastToJson(DayForecast instance) =>
    <String, dynamic>{
      'dt': instance.dt,
      'main': instance.main?.toJson(),
      'weather': instance.weather?.map((e) => e?.toJson())?.toList(),
      'clouds': instance.clouds?.toJson(),
      'wind': instance.wind?.toJson(),
      'visibility': instance.visibility,
      'pop': instance.pop,
      'rain': instance.rain?.toJson(),
      'sys': instance.sys?.toJson(),
      'dt_txt': instance.dtTxt?.toIso8601String(),
    };
