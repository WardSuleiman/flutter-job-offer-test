import 'package:json_annotation/json_annotation.dart';

import 'package:flutter_job_test/core/network/models/cloud.dart';
import 'package:flutter_job_test/core/network/models/main.dart';
import 'package:flutter_job_test/core/network/models/rain.dart';
import 'package:flutter_job_test/core/network/models/time_of_day.dart';
import 'package:flutter_job_test/core/network/models/weather.dart';
import 'package:flutter_job_test/core/network/models/wind.dart';

part 'day_forecast.g.dart';

@JsonSerializable(explicitToJson: true)
class DayForecast {
  int dt;
  Main main;
  List<Weather> weather;
  Cloud clouds;
  Wind wind;
  int visibility;
  double pop;
  Rain rain;
  TimeOfDay sys;
  @JsonKey(name: 'dt_txt', fromJson: _convertTODateTime)
  DateTime dtTxt;

  DayForecast({
    this.dt,
    this.main,
    this.weather,
    this.clouds,
    this.wind,
    this.visibility,
    this.pop,
    this.rain,
    this.sys,
    this.dtTxt,
  });

  factory DayForecast.fromJson(Map<String, dynamic> json) =>
      _$DayForecastFromJson(json);
  Map<String, dynamic> toJson() => _$DayForecastToJson(this);

  @override
  String toString() {
    return 'DayForecast(dt: $dt, main: $main, weather: $weather, clouds: $clouds, wind: $wind, visibility: $visibility, pop: $pop, rain: $rain, sys: $sys, dtTxt: $dtTxt)';
  }

  static _convertTODateTime(dynamic json){
    DateTime _dateTime = DateTime.parse(json);
    return _dateTime;
  }

}
