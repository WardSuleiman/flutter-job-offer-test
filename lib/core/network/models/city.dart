import 'package:json_annotation/json_annotation.dart';

import 'package:flutter_job_test/core/network/models/coordination.dart';

part 'city.g.dart';

@JsonSerializable(explicitToJson: true)
class City {
  int id;
  String name;
  Coordination coord;
  String country;
  int population;
  int timezone;
  int sunrise;
  int sunset;

  City(
      {this.id,
      this.name,
      this.coord,
      this.country,
      this.population,
      this.timezone,
      this.sunrise,
      this.sunset});

  factory City.fromJson(Map<String, dynamic> json) => _$CityFromJson(json);
  Map<String, dynamic> toJson() => _$CityToJson(this);

  @override
  String toString() {
    return 'City(id: $id, name: $name, coord: $coord, country: $country, population: $population, timezone: $timezone, sunrise: $sunrise, sunset: $sunset)';
  }
}
