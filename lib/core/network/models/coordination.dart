import 'package:json_annotation/json_annotation.dart';

part 'coordination.g.dart';

@JsonSerializable()
class Coordination {
  double lat;
  double lon;

  Coordination({
    this.lat,
    this.lon,
  });

  factory Coordination.fromJson(Map<String, dynamic> json) =>
      _$CoordinationFromJson(json);
  Map<String, dynamic> toJson() => _$CoordinationToJson(this);

  @override
  String toString() => 'Coordination(lat: $lat, lon: $lon)';
}
