// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rain.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Rain _$RainFromJson(Map<String, dynamic> json) {
  return Rain(
    d3h: (json['3h'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$RainToJson(Rain instance) => <String, dynamic>{
      '3h': instance.d3h,
    };
