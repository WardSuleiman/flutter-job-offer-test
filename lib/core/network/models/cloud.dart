import 'package:json_annotation/json_annotation.dart';

part 'cloud.g.dart';

@JsonSerializable()
class Cloud {
  int all;

  Cloud({
    this.all,
  });

  factory Cloud.fromJson(Map<String, dynamic> json) => _$CloudFromJson(json);
  Map<String, dynamic> toJson() => _$CloudToJson(this);

  @override
  String toString() => 'Cloud(all: $all)';
}
