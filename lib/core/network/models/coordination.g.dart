// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coordination.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Coordination _$CoordinationFromJson(Map<String, dynamic> json) {
  return Coordination(
    lat: (json['lat'] as num)?.toDouble(),
    lon: (json['lon'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$CoordinationToJson(Coordination instance) =>
    <String, dynamic>{
      'lat': instance.lat,
      'lon': instance.lon,
    };
