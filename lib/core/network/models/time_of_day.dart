import 'package:json_annotation/json_annotation.dart';

part 'time_of_day.g.dart';

@JsonSerializable()
class TimeOfDay {
  String pod;

  TimeOfDay({
    this.pod,
  });

  factory TimeOfDay.fromJson(Map<String, dynamic> json) =>
      _$TimeOfDayFromJson(json);
  Map<String, dynamic> toJson() => _$TimeOfDayToJson(this);

  @override
  String toString() => 'TimeOfDay(pod: $pod)';
}
