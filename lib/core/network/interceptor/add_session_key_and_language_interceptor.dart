import 'dart:async';
import 'dart:io';

import 'package:chopper/chopper.dart';
import 'package:flutter_job_test/core/constants/open_weathe_api_info.dart';

class AddApiKeyAndLanguageKeyInterceptor implements RequestInterceptor {
  @override
  FutureOr<Request> onRequest(Request request) {
    final _temp = {
      ...request.parameters,
      'appid': api_key,
      'lang': Platform.localeName.contains('ar') ? 'ar' : 'en' ?? 'en',
      'mode': 'json',
      'units': 'metric',
      'cnt': 40,
    };
    var v = request.copyWith(parameters: _temp);
    return v;
  }
}
