import 'package:flutter/material.dart';
import 'package:flutter_job_test/weather/presentation/providers/weather_provider.dart';
import 'package:logging/logging.dart';
import 'package:flutter_job_test/core/injection/locator.dart';
import 'package:flutter_job_test/weather/presentation/pages/main_weather_page.dart';
import 'package:provider/provider.dart';

void _setupLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen(
    (rec) {
      print('${rec.level.name}: ${rec.time}: ${rec.message}');
    },
  );
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  _setupLogging();
  await setupLocator();

  /* WeatherRepository _weatherRepository = locator<WeatherRepository>();

  final r =
      await _weatherRepository.getTodayAndFiveDaysFromNowForecast('Damascus');

  print(r.data);
  print(r.errors); */
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => WeatherProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Weather Forecast',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.orange,
        ),
        home: MainWeatherPage(),
      ),
    );
  }
}
