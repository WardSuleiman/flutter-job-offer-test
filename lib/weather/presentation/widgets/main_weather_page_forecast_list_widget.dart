import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:flutter_job_test/weather/presentation/providers/weather_provider.dart';
import 'package:flutter_job_test/weather/presentation/widgets/main_weather_page_day_forecast_widget.dart';

class MainWeatherPageForecastListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _weatherForecast = context.read<WeatherProvider>().weatherForecast;
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: _weatherForecast.length,
      itemBuilder: (_, int index) => MainWeatherPageDayForeCastWidget(
        dayForecast: _weatherForecast[index],
      ),
    );
  }
}
