import 'package:flutter/material.dart';
import 'package:flutter_job_test/core/size/size_config.dart';

class WeatherDetailsPageSilverAppBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      floating: true,
      stretch: true,
      expandedHeight: SizeConfig.screenHeight * 0.3,
      flexibleSpace: Center(
        child: FlexibleSpaceBar(
          stretchModes: const <StretchMode>[
            StretchMode.zoomBackground,
          ],
          title: Text(
            'Details page',
            textAlign: TextAlign.center,
          ),
          centerTitle: true,
          background: DecoratedBox(
            position: DecorationPosition.foreground,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.center,
                colors: <Color>[
                  Colors.teal[800],
                  Colors.transparent,
                ],
              ),
            ),
            child: Image.network(
              'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl-2.jpg',
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}
