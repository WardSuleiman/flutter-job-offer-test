import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:flutter_job_test/core/network/models/day_forecast.dart';
import 'package:flutter_job_test/core/size/size_config.dart';
import 'package:flutter_job_test/weather/presentation/pages/weather_details_page.dart';
import 'package:flutter_job_test/weather/presentation/providers/day_forecast_provider.dart';

class MainWeatherPageDayForeCastWidget extends StatelessWidget {
  final DayForecast dayForecast;

  MainWeatherPageDayForeCastWidget({
    @required this.dayForecast,
  });

  final _dateFormat = DateFormat('yy-MM-dd');
  final _dateFormat1 = DateFormat('kk:mm');

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: SizeConfig.screenHeight * 0.22,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Colors.grey, Colors.white, Colors.orange],
            ),
          ),
          child: Card(
            color: Colors.transparent,
            elevation: 50,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CircleAvatar(
                          radius: SizeConfig.screenWidth * 0.07,
                          child: AutoSizeText(
                            dayForecast.main.temp.toString(),
                          ),
                        ),
                        CircleAvatar(
                          radius: SizeConfig.screenWidth * 0.07,
                          child: AutoSizeText(
                            dayForecast.weather[0].main,
                            maxLines: 1,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        AutoSizeText(
                          _dateFormat.format(dayForecast.dtTxt),
                          style: Theme.of(context).textTheme.headline5,
                        ),
                        AutoSizeText(
                          'Time Recorded ' +
                              _dateFormat1.format(dayForecast.dtTxt),
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                        AutoSizeText(
                          'Humidity  ${dayForecast.main.humidity}%',
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                        AutoSizeText(
                          'Wind Speed ${dayForecast.wind.speed} kmh',
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                        AutoSizeText(
                          'Cloud ${dayForecast.clouds.all}%',
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                      ],
                    ),
                  ),
                  Image.network(
                    'https://openweathermap.org/img/wn/${dayForecast.weather[0].icon}@4x.png',
                    fit: BoxFit.cover,
                    loadingBuilder: (BuildContext context, Widget child,
                        ImageChunkEvent loadingProgress) {
                      if (loadingProgress == null) return child;
                      return CircularProgressIndicator.adaptive(
                        value: loadingProgress.expectedTotalBytes != null
                            ? loadingProgress.cumulativeBytesLoaded /
                                loadingProgress.expectedTotalBytes
                            : null,
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      onTap: () => Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => ChangeNotifierProvider(
            create: (context) => DayForecastProvider(
              dayForecast: dayForecast,
            ),
            child: WeatherDetailsPage(),
          ),
        ),
      ),
    );
  }
}
