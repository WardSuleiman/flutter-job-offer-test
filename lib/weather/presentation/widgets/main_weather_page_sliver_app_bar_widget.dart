import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_job_test/weather/presentation/providers/weather_provider.dart';
import 'package:provider/provider.dart';
import 'package:flutter_job_test/core/size/size_config.dart';

class MainWeatherPageSliverAppBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _weatherProvider = context.read<WeatherProvider>();
    return SliverAppBar(
      pinned: true,
      floating: true,
      stretch: true,
      expandedHeight: SizeConfig.screenHeight * 0.3,
      flexibleSpace: Center(
        child: FlexibleSpaceBar(
          stretchModes: const <StretchMode>[
            StretchMode.zoomBackground,
          ],
          title: AutoSizeText(
            'weather forecast for the next 5 days in ${_weatherProvider.city.name}-${_weatherProvider.city.country}',
            textAlign: TextAlign.center,
          ),
          centerTitle: true,
          background: DecoratedBox(
            position: DecorationPosition.foreground,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.center,
                colors: <Color>[
                  Theme.of(context).primaryColor,
                  Colors.transparent,
                ],
              ),
            ),
            child: Image.asset(
              'assets/images/all_seasons.jpg',
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}
