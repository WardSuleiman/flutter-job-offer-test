import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import 'package:flutter_job_test/weather/presentation/providers/day_forecast_provider.dart';
import 'package:flutter_job_test/weather/presentation/providers/weather_provider.dart';
import 'package:provider/provider.dart';

class WeatherDetailsPage extends StatelessWidget {
  String selectBackgroundImage(String weatherCondition) {
    String _selectedImage;
    switch (weatherCondition) {
      case 'Clear':
        _selectedImage = 'assets/images/sunny.jpg';
        break;
      case 'Clouds':
        _selectedImage = 'assets/images/cloudy.jpg';
        break;
      case 'Snow':
      case 'Rain':
        _selectedImage = 'assets/images/snowy.jpg';
        break;
      default:
        _selectedImage = 'assets/images/sunny.jpg';
        break;
    }
    return _selectedImage;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<DayForecastProvider>(
      builder: (_, DayForecastProvider dayForecastProvider, __) => Scaffold(
        body: LayoutBuilder(
          builder: (_, BoxConstraints constraints) => Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                      selectBackgroundImage(
                          dayForecastProvider.dayForecast.weather[0].main),
                    ),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              PositionedDirectional(
                start: constraints.maxWidth * 0.1,
                top: constraints.maxWidth * 0.2,
                child: Container(
                  height: constraints.maxHeight * 0.1,
                  width: constraints.maxWidth * 0.4,
                  child: Center(
                    child: AutoSizeText(
                      dayForecastProvider.dayForecast.weather[0].main,
                      minFontSize: 40,
                      maxFontSize: 60,
                    ),
                  ),
                ),
              ),
              PositionedDirectional(
                start: constraints.maxWidth * 0.05,
                bottom: constraints.maxWidth * 0.15,
                child: Row(
                  children: [
                    Column(
                      children: [
                        Container(
                          height: constraints.maxHeight * 0.1,
                          width: constraints.maxWidth * 0.4,
                          child: Center(
                            child: AutoSizeText(
                              context.read<WeatherProvider>().city.name,
                              minFontSize: 60,
                              maxFontSize: 80,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          height: constraints.maxHeight * 0.1,
                          width: constraints.maxWidth * 0.4,
                          child: Center(
                            child: AutoSizeText(
                              dayForecastProvider.dayForecast.main.temp
                                  .toStringAsFixed(0),
                              minFontSize: 60,
                              maxFontSize: 80,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: constraints.maxHeight * 0.1,
                        width: constraints.maxWidth * 0.2,
                        color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: AutoSizeText(
                                  'Wind',
                                  minFontSize: 20,
                                  maxFontSize: 40,
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: AutoSizeText(
                                  dayForecastProvider.dayForecast.wind.speed
                                          .toStringAsFixed(0) +
                                      'km/h',
                                  minFontSize: 20,
                                  maxFontSize: 40,
                                  maxLines: 1,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: constraints.maxHeight * 0.12,
                        width: constraints.maxWidth * 0.24,
                        color: Colors.white,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: AutoSizeText(
                                  'Humidity',
                                  minFontSize: 20,
                                  maxFontSize: 40,
                                  maxLines: 1,
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: AutoSizeText(
                                  '%' +
                                      dayForecastProvider
                                          .dayForecast.main.humidity
                                          .toString(),
                                  minFontSize: 20,
                                  maxFontSize: 40,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
