import 'package:flutter/material.dart';
import 'package:flutter_job_test/core/network/base_response.dart';
import 'package:flutter_job_test/core/network/models/day_forecast.dart';
import 'package:provider/provider.dart';

import 'package:flutter_job_test/core/size/size_config.dart';
import 'package:flutter_job_test/weather/presentation/providers/weather_provider.dart';
import 'package:flutter_job_test/weather/presentation/widgets/main_weather_page_forecast_list_widget.dart';
import 'package:flutter_job_test/weather/presentation/widgets/main_weather_page_sliver_app_bar_widget.dart';

class MainWeatherPage extends StatefulWidget {
  @override
  _MainWeatherPageState createState() => _MainWeatherPageState();
}

class _MainWeatherPageState extends State<MainWeatherPage> {
  Future _future;

  @override
  void initState() {
    _future = context
        .read<WeatherProvider>()
        .getWeatherForecastFor5daysFromNow('Damascus');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig _sizeConfig = SizeConfig();
    _sizeConfig.init(context);
    return Scaffold(
      body: FutureBuilder<BaseResponse<List<DayForecast>>>(
        future: _future,
        builder: (_, AsyncSnapshot snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return Center(
              child: LinearProgressIndicator(),
            );
          } else {
            if (snapshot.data.data != null) {
              return CustomScrollView(
                physics: const BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics(),
                ),
                slivers: [
                  MainWeatherPageSliverAppBarWidget(),
                  SliverList(
                    delegate: SliverChildListDelegate.fixed(
                      [
                        MainWeatherPageForecastListWidget(),
                      ],
                    ),
                  )
                ],
              );
            } else {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: ElevatedButton(
                      child: Text('Retry'),
                      onPressed: () => setState(() {
                        _future = context
                            .read<WeatherProvider>()
                            .getWeatherForecastFor5daysFromNow('Damascus');
                      }),
                    ),
                  ),
                ],
              );
            }
          }
        },
      ),
    );
  }
}
