import 'package:flutter/material.dart';
import 'package:flutter_job_test/core/injection/locator.dart';
import 'package:flutter_job_test/core/network/base_response.dart';
import 'package:flutter_job_test/core/network/models/city.dart';
import 'package:flutter_job_test/core/network/models/day_forecast.dart';
import 'package:flutter_job_test/weather/data/repository/weather_repository.dart';

class WeatherProvider with ChangeNotifier {
  final WeatherRepository _weatherRepository = locator<WeatherRepository>();

  List<DayForecast> weatherForecast = [];
  City city;

  Future<BaseResponse<List<DayForecast>>> getWeatherForecastFor5daysFromNow(
      String cityName) async {
    final _baseResponse =
        await _weatherRepository.getTodayAndFiveDaysFromNowForecast(
      cityName,
    );
    if (_baseResponse.status == Status.COMPLETED) {
      this.weatherForecast = _baseResponse.data;

      weatherForecast = _getOnly6DaysResults(weatherForecast);
      city = _baseResponse.city;

      return BaseResponse.completed(weatherForecast, _baseResponse.city);
    } else {
      return BaseResponse.error(null, _baseResponse.errors);
    }
  }

  List<DayForecast> _getOnly6DaysResults(List<DayForecast> listDayForecast) {
    List<DayForecast> _selectedList = [];
    DateTime _currentDateTime;
    listDayForecast.forEach(
      (dayForecast) {
        if (_currentDateTime == null) {
          _currentDateTime = dayForecast.dtTxt;
          _selectedList.add(dayForecast);
        } else {
          if (dayForecast.dtTxt.day != _currentDateTime.day) {
            _currentDateTime = dayForecast.dtTxt;
            _selectedList.add(dayForecast);
          }
        }
      },
    );
    return _selectedList;
  }
}
