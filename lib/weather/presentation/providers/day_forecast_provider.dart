import 'package:flutter/material.dart';
import 'package:flutter_job_test/core/network/models/day_forecast.dart';

class DayForecastProvider with ChangeNotifier {
  final DayForecast dayForecast;

  DayForecastProvider({this.dayForecast});
}
