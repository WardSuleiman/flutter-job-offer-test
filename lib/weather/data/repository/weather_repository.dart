import 'package:flutter_job_test/core/network/base_response.dart';
import 'package:flutter_job_test/core/network/models/day_forecast.dart';

abstract class WeatherRepository {
  Future<BaseResponse<List<DayForecast>>> getTodayAndFiveDaysFromNowForecast(
      String cityName);
}
