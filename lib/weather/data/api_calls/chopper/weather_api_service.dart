import 'package:chopper/chopper.dart';

part 'weather_api_service.chopper.dart';

@ChopperApi(baseUrl: '/forecast')
abstract class WeatherApiService extends ChopperService {
  static WeatherApiService create([ChopperClient client]) =>
      _$WeatherApiService(client);

  @Get()
  Future<Response> get5DaysForecast(
    @QueryMap() Map<String, dynamic> query,
  );
}
