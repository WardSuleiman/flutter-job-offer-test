import 'package:flutter_job_test/core/network/api_service.dart';
import 'package:flutter_job_test/core/network/base_api_calls.dart';
import 'package:flutter_job_test/core/network/error/exceptions.dart';
import 'package:flutter_job_test/core/network/models/day_forecast.dart';
import 'package:flutter_job_test/core/network/base_response.dart';
import 'package:flutter_job_test/weather/data/repository/weather_repository.dart';
import 'package:flutter_job_test/weather/data/api_calls/chopper/weather_api_service.dart';

class WeatherApiCalls implements WeatherRepository {
  final WeatherApiService _weatherApiService =
      WeatherApiService.create(ApiService.chopper);

  @override
  Future<BaseResponse<List<DayForecast>>> getTodayAndFiveDaysFromNowForecast(
    String cityName,
  ) async {
    try {
      final BaseResponse _baseResponse = await apiCall(
        () async => _weatherApiService.get5DaysForecast(
          {
            'id': 292223,
          },
        ),
      );
      if (_baseResponse.status == Status.COMPLETED) {
        List<dynamic> _listOfForecasts = _baseResponse.data;
        List<DayForecast> _forecasts = _listOfForecasts
            .map(
              (forecast) => DayForecast.fromJson(forecast),
            )
            .toList();
        return BaseResponse.completed(_forecasts, _baseResponse.city);
      } else {
        throw BaseException(
          message: 'Something unknown happened',
          type: 'Unknown',
        );
      }
    } on BaseException catch (e) {
      return BaseResponse.error(
        null,
        [
          BaseResponseError(
            errorName: e.type,
            errorMessage: e.message,
          ),
        ],
      );
    }
  }
}
